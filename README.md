HydroSim FMOD Project
=====================

FMOD Studio Version: `2.02.16`

This repository contains the FMOD Studio Project used to create sounds for HydroSim.

By default FMOD Banks are loaded in through HydroSim's boat bundles, however they can be overridden. To override the default sounds, the FMOD Banks can be built into the `HydroSim/HydroSim_Data/StreamingAssets/Audio` directory.

FMOD Events are organized as following.

Each folder is mapped to the hull name. The game only loads the `Main` event for each hull. All other events are assigned to and triggered by the `Main` event.

Events under common are not specific to any hull.

1. **Collision** - Events are played when the boat collides with various materials.
2. **Underwater** - Event is played when the camera is underwater.
3. **Wind** - Event is played all the time, only affected by boat speed.

To Build the Bank files, just goto File -> Build. By default this will
place the files into a `Build` folder in the HydroSim FMOD Project's
folder.

The Build path can be modified to build directly into HydroSim's
StreamingAssets folder.

Go to Preferences:

![Preferences](docs/preferences.png)

Switch to the Build Tab:

![Build Tab](docs/build.png)

Click browse and navigate to your HydroSim `StreamingAssets/Audio` folder
and click `Select Folder`:

![Select Folder](docs/selectfolder.png)
![Folder Selected](docs/folderselected.png)

Now when you build the FMOD project the bank files will get picked up
when you restart HydroSim.
